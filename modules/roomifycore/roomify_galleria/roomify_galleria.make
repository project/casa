; API
api = 2
core = 7.x

; Galleria library
libraries[galleria][download][type] = "file"
libraries[galleria][download][url] = "http://galleria.io/static/galleria-1.3.5.zip"
libraries[galleria][directory_name] = "galleria"
libraries[galleria][type] = "library"
